package com.exa.pms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * This class contains Database related configuration details.
 * 
 * @author Dillip.Nayak
 *
 */
@Configuration
@EnableTransactionManagement
@EnableLoadTimeWeaving(aspectjWeaving = EnableLoadTimeWeaving.AspectJWeaving.ENABLED)
public class PersistenceConfig {

	/**
	 * In this method we defining the path of EntityManagerFactoryBean and it
	 * will create the EntityManager implementation class object.
	 * 
	 * @return EntityManager implementation object.
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean getEntityManagerFactoryBean() {
		LocalContainerEntityManagerFactoryBean lcemfb = new LocalContainerEntityManagerFactoryBean();
		lcemfb.setPersistenceXmlLocation("classpath:META-INF/persistence.xml");
		lcemfb.setPersistenceUnitName("pms-web");
		LoadTimeWeaver loadTimeWeaver = new InstrumentationLoadTimeWeaver();
		lcemfb.setLoadTimeWeaver(loadTimeWeaver);
		return lcemfb;
	}

	/**
	 * To configure JPATransaction Manager.
	 * 
	 * @return JpaTransactionManeger object.
	 */
	
}
