package com.exa.pms.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.exa.pms.dao.FileUploadDAO;
import com.exa.pms.daoimpl.FileUploadDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * DispatcherServlet Context: defines this servlet's request-processing
 * infrastructure
 * 
 * @author Dillip.Nayak
 *
 */
// Treat as the configuration file for Spring MVC-enabled applications.

@Configuration
// Enable Spring MVC-specific annotations like @Controller
@EnableWebMvc
// Scan starts from base package and registers all controllers, repositories,
// service, beans, etc.
@ComponentScan(basePackages = { "com.exa.pms" })
public class WebConfig extends WebMvcConfigurerAdapter {

	private int maxUploadSizeInMb = 5 * 1024 * 1024; // 5 MB

	/**
	 * Resolves views selected for rendering by @Controllers
	 */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	/**
	 * To configure the Gson Object.
	 * 
	 * @return
	 */
	@Bean
	public Gson gson() {
		GsonBuilder gb = new GsonBuilder();
		gb.serializeNulls();
		return gb.create();
	}


	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver getCommonsMultipartResolver() {
	    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	    multipartResolver.setMaxUploadSize(20971520);   // 20MB
	    multipartResolver.setMaxInMemorySize(1048576);  // 1MB
	    return multipartResolver;
	}
	
	
	
}