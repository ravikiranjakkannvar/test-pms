package com.exa.pms.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.model.AddProjectDetails;
import com.exa.pms.service.AddProjectDetailsService;
import com.exa.pms.service.ProjectService;





@RestController
@RequestMapping("/projectdetails")
public class AddProjectDetailsController {
	
	
	@Autowired
	private AddProjectDetailsService addProjDetailsService;
	
	/*
	 * fetching Project details from database. 
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="getprojectdetails", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<AddProjectDetails> getAddProjectDetails(Map model, HttpSession session) {
		System.out.println("Am in controller class of qresult1");
		return addProjDetailsService.getProjectDetails();
	}
	
	/*
	 * inserting project details into database and validating.
	 */
	
	@RequestMapping(value="insertProjectDetails" , method = RequestMethod.POST)
	public @ResponseBody boolean  insertAddProjectDetals(@RequestBody String addProDetails)
	{
		System.out.println("am in insert()");
		String a = addProjDetailsService.insertProjectDetails(addProDetails);
		
		if(a == "success")
			return true;
		else 
			return false;
	}
	
	/*
	 * updating project details into database.
	 */
	
	
	@RequestMapping(value = "updateProjectDetails", method = RequestMethod.POST)
	public @ResponseBody boolean updateParticipantReport(@RequestBody String addProjDetails, Map model, HttpSession session) {
		System.out.println("method invked level 1");
		addProjDetailsService.updateProjectDetails(addProjDetails);
		return true;
	}
	
}
