package com.exa.pms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.model.AddSprint;
import com.exa.pms.service.AddSprintService;

/**
 * AddSprintController class is responsible for accepting the request from user.
 * 
 * @author Pratima.Dolli
 *
 */

@RestController
public class AddSprintController {

	/*
	 * Autowired enables we to inject the object dependency implicitly. 
	 * It internally uses setter or constructor injection.
	 */
	
	@Autowired
	private AddSprintService addSprintService;
	
	/*
	 * inserting AddSprint details into database
	 */
	
	@PostMapping("/insertSprintInfo")
	public AddSprint addSprint(@RequestBody AddSprint addinfo)
	{
		System.out.println("controller class");
		
		return addSprintService.addSprintDetails(addinfo);
		
	}
	
	/*
	 * Fetching AddSprint details from database
	 */
	
	@GetMapping("/getSprintInfo")
	public List<AddSprint> getList()
	{
		return addSprintService.getSprintDetails();
		
	}
	
}
