package com.exa.pms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.service.BackLogService;

@RestController
@RequestMapping("/backlogs")
public class BackLogController {
	
	@Autowired
	private BackLogService backLogService;
	
	
	@RequestMapping(value="insertBackLogDetails" , method = RequestMethod.POST)
	public @ResponseBody boolean  insertAddBacklogDetals(@RequestBody String addBacklogDetails)
	{
		System.out.println("am in insert()");
		String a = backLogService.insertBacklogDetails(addBacklogDetails);
		
		if(a == "success")
			return true;
		else 
			return false;
	}
	

}
