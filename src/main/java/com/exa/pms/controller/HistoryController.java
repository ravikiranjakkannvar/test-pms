package com.exa.pms.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.model.History;
import com.exa.pms.service.HistoryService;
import com.exa.pms.service.UserLoginService;


/**
 * @author Sireesha.Palagiri
 *
 */
@RestController
@RequestMapping("/history")
public class HistoryController {

	@Autowired
	private HistoryService historyService;

	/*
	 * inserting recent changes into database
	 */
	@RequestMapping(value = "/insertchanges", method = RequestMethod.POST)
	public @ResponseBody boolean insertHistory(@RequestBody String changes) {
		String status = historyService.insertHistory(changes);
		if (status == "success") {
			return true;
		} else
			return false;
	}
	/*
	 * fetching recent changes from database
	 */

	@RequestMapping(value = "/gethistory", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<History> getHistory(Map model, HttpSession session) {
		return historyService.getHistory();
	}

}
