package com.exa.pms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.exa.pms.service.ProjectService;
import com.google.gson.Gson;

/**
 * ProjectController class is responsible accepting the request of user.
 * 
 * @author Dillip.Nayak
 *
 */
@Controller
public class ProjectController {

	// It will Inject the ProjectService object
	@Autowired
	ProjectService projectService;

	// It will Inject the Gson object
	@Autowired
	Gson gson;

	/**
	 * It is used to select the project name to render the project details .
	 * returns the project details in Json format.
	 */
	@RequestMapping(value = "/projectname", method = RequestMethod.GET)
	public @ResponseBody String showProject() {
		System.out.println("Dj");
		return gson.toJson(projectService.getAllAdmin());
	}
}
