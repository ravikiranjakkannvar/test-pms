package com.exa.pms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.model.Roles;
import com.exa.pms.service.RolesService;

/**
 * 
 * @author Pratima.Dolli
 *
 */
@RestController
public class RolesController {


	/*
	 * Autowired enables we to inject the object dependency implicitly. 
	 * It internally uses setter or constructor injection.
	 */
	
	@Autowired
	private RolesService rolesService;
	
	/*
	 * inserting AddSprint details into database
	 */
	
	@PostMapping("/insertRoles")
	public Roles save(@RequestBody Roles role)
	{
		return rolesService.insertRoles(role);		
	}
}
