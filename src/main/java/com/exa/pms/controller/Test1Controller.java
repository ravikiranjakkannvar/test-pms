/**
 * 
 */
package com.exa.pms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.model.Test1;
import com.exa.pms.service.Test1Service;

/**
 * @author Ravikiran J
 *
 */
@RestController
public class Test1Controller {



	@Autowired
	private Test1Service testService;

	@PostMapping("/test1insert")
	public boolean save(@RequestBody Test1 test) {
		testService.createTest1(test);
		return true;
	}

	@GetMapping("/test1s")
	public List<Test1> getList(){
		return testService.getList();
	}

	@GetMapping("/test1/{id}")
	public Test1 getOne(@PathVariable Integer id) {
		Test1 test=testService.findById(id);
		return test;
	}

	@PutMapping("/updatetest1")
	public boolean update(@RequestBody Test1 test) {
		testService.update(test);
		return true;
	}

	@DeleteMapping("/deletetest1/{id}")
	public boolean delete(@RequestBody Test1 test,@PathVariable Integer id) {
		Test1 test1=testService.findById(id);
		if(test1!=null) {
			testService.delete(test);
			return true;	
		}
		return false;

	}

}
