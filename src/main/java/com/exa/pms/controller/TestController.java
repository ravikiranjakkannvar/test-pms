/**
 * 
 */
package com.exa.pms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.dao.TestDao;
import com.exa.pms.model.Test;
import com.exa.pms.service.TestService;
import com.exa.pms.serviceimpl.TestServiceImpl;

/**
 * @author Ravikiran J
 *
 */
@RestController
public class TestController {

	
	
	@Autowired
	private TestService testService;

	@PostMapping("/testinsert")
	public boolean save(@RequestBody Test test) {
		testService.createTest(test);
		return true;
	}

	@GetMapping("/tests")
	public List<Test> getList(){
		return testService.getList();
	}

	@GetMapping("/test/{id}")
	public Test getOne(@PathVariable Integer id) {
		Test test=testService.findById(id);
		return test;
	}

	@PutMapping("/updatetest")
	public boolean update(@RequestBody Test test) {
		testService.update(test);
		return true;
	}

	@DeleteMapping("/deletetest/{id}")
	public boolean delete(@RequestBody Test test,@PathVariable Integer id) {
		Test test1=testService.findById(id);
		if(test1!=null) {
			testService.delete(test);
			return true;	
		}
		return false;

	}

}
