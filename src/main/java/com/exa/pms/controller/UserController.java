package com.exa.pms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exa.pms.model.User;
import com.exa.pms.service.UserLoginService;
import com.exa.pms.service.UserService;

/**
 * @author Sireesha.Palagiri
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserLoginService userLoginService;

	/*
	 * inserting user details into database
	 */
	@RequestMapping(value = "/insertuser", method = RequestMethod.POST)
	public @ResponseBody boolean insertUser(@RequestBody String user) {
		String status = userService.insertUser(user);
		if (status == "success") {
			return true;
		} else
			return false;
	}

	/*
	 * Validating login details based on email and password
	 */
	@RequestMapping(value = "/validateuser", method = RequestMethod.POST)
	public @ResponseBody String validateUser(@RequestBody String logindetails) {
		String status = userLoginService.validateUser(logindetails);
		if (status == "not exist") {
			return "not exist";
		} else if (status == "password incorrect") {
			return "password incorrect";
		} else if (status == "success") {
			return "true";
		} else
			return "false";
	}


	@PutMapping("/updateuser")
	public boolean update(@RequestBody User user)
	{
		userService.update(user);
		return true;
	}

}
