package com.exa.pms.dao;

import java.util.List;

import com.exa.pms.model.AddProjectDetails;


public interface AddProjectDetailsDao {
	
	
		/*
		 * fetching all Project details from database
		 */
		
		public  List<AddProjectDetails> getProjectDetailsList();
		
		
		/*
		 * inserting Project details data into database and validation
		 */
		
		
		String insertProjectDetails(AddProjectDetails projDetails);
		
		/*
		 * updating Project details data into database
		 */
		
		void updateProjectDetails(AddProjectDetails projDetails);
		
				

}
