package com.exa.pms.dao;

import java.util.List;

import com.exa.pms.model.AddSprint;
/**
 * 
 * @author Pratima.Dolli
 *
 */
public interface AddSprintDao {

	/**
	 * inserting sprint details into database
	 * 
	 * @param addinfo
	 * @return
	 */
	public AddSprint addSprintDetails(AddSprint addinfo);

	/*
	 * getting sprint details from database
	 */
	public List<AddSprint> getSprintDetails();

}
