package com.exa.pms.dao;

import com.exa.pms.model.BackLog;

public interface BackLogDao {

	String insertBacklogDetails(BackLog backLog);

}
