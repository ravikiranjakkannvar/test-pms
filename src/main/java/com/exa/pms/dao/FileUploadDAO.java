/**
 * 
 */
package com.exa.pms.dao;

import com.exa.pms.model.UploadFile;

/**
 * @author Ravikiran J
 *
 */
public interface FileUploadDAO {


	void save(UploadFile uploadFile);


}
