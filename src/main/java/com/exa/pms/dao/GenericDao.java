/**
 * 
 */
package com.exa.pms.dao;

import java.util.List;

/**
 * @author Ravikiran J
 *
 *Creating GenericDao and passing model as T and respected model class ID
 */
public interface GenericDao<T,ID> {

	//list
	List<T> findAll();
	//save
	T save(T entity);
	//update
	T update( T entity);
	//delete
	void delete(T entity);
	//findById
	T findById(ID id);
	//flush
	void flush();
	//delete By Id 
	void deleteById(ID id);

}
