package com.exa.pms.dao;

import java.util.List;

import com.exa.pms.model.History;

/**
 * @author Sireesha.Palagiri
 *
 */
public interface HistoryDao {
	// inserting recent changes into database
	String insertHistory(History history);
	List<History> getHistory();
}
