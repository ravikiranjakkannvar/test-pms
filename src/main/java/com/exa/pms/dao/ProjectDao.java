package com.exa.pms.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.exa.pms.model.Project;

/**
 * 
 * @author Dillip.Nayak
 *
 */
@Repository("projectDao")
public class ProjectDao {
	
	//
	@PersistenceContext
	private EntityManager oEntityManager;

	/**
	 * 
	 * @return
	 */
	public List<Project> getAllEmployee() {
		return oEntityManager.createQuery("select p from Project p", Project.class).getResultList();
	}
}
