package com.exa.pms.dao;

import com.exa.pms.model.Roles;
/**
 * 
 * @author Pratima.Dolli
 *
 */
public interface RolesDao {

	/*
	 * inserting AddSprint details into database
	 */
	
	public Roles insertRoles(Roles role);

}
