/**
 * 
 */
package com.exa.pms.dao;

import com.exa.pms.model.Test1;

/**
 * @author Ravikiran J
 *
 */
public interface Test1Dao extends GenericDao<Test1, Integer> {

	public void delete(Integer id);

}
