/**
 * 
 */
package com.exa.pms.dao;

import com.exa.pms.model.Test;

/**
 * @author Ravikiran J
 *
 */
public interface TestDao extends GenericDao<Test, Integer>{

	
	
}
