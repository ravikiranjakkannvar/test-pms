package com.exa.pms.dao;

import java.util.List;

import com.exa.pms.model.User;

/**
 * @author Sireesha.Palagiri
 *
 */
@SuppressWarnings("unused")
public interface UserDao {

	// inserting user details into database
	String insertUser(User user);
	//update
	public int update(User user);
}
