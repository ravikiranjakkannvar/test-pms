package com.exa.pms.dao;

import com.exa.pms.model.User;

/**
 * @author Sireesha.Palagiri
 *
 */
public interface UserLoginDao {
	//validating login details 
	String validateUser(User user);
}
