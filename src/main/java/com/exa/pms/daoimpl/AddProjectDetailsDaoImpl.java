package com.exa.pms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.exa.pms.dao.AddProjectDetailsDao;
import com.exa.pms.model.AddProjectDetails;
import com.exa.pms.model.User;



@Repository("addprojectDetailsDao")
public class AddProjectDetailsDaoImpl implements AddProjectDetailsDao {

	@PersistenceContext
	private EntityManager em;
	
	/*
	 * Method to get the details of projects
	 * @see com.exa.pms.dao.AddProjectDetailsDao#getProjectDetailsList()
	 */
	@Override
	public List<AddProjectDetails> getProjectDetailsList() {
		System.out.println("am in qresult daoimpl");
		System.out.println((List<AddProjectDetails>) em
				.createQuery("select q from AddProjectDetails q", AddProjectDetails.class).getResultList());
		return em.createQuery("select q from AddProjectDetails q", AddProjectDetails.class).getResultList();
	}

	/*
	 *  Method to insert the details of projects
	 * @see com.exa.pms.dao.AddProjectDetailsDao#insertProjectDetails(com.exa.pms.model.AddProjectDetails)
	 */
	
	
	@Override
	public String insertProjectDetails(AddProjectDetails addprojDetails) {
		
		String res = "fail";
		Query query = em.createQuery("select q from AddProjectDetails q where q.projectName=:projectName ", AddProjectDetails.class);
		@SuppressWarnings("unchecked")
		List<AddProjectDetails> projectName = (List<AddProjectDetails>) query.setParameter("projectName", addprojDetails.getProjectName()).getResultList();
		if(projectName.size() > 0){
			res = "fail";
		}else{
			System.out.println("invoking Dao Class");
			em.persist(addprojDetails);
			System.out.println("success");
			res = "success";
		}
		return res;
				
	}

	/*
	 *  Method to update the details of projects
	 * @see com.exa.pms.dao.AddProjectDetailsDao#insertProjectDetails(com.exa.pms.model.AddProjectDetails)
	 */
	@Override
	public void updateProjectDetails(AddProjectDetails projDetails) {
		Query query = em.createQuery("UPDATE AddProjectDetails q SET q.projectManager = :projectManager, q.startDate =:startDate, q.estimationEndDate =:estimationEndDate, q.creationDate=:creationDate, q.scrumMaster =:scrumMaster WHERE q.projectName=:projectName ", AddProjectDetails.class);
		query.setParameter("creationDate", projDetails.getCreationDate());
		query.setParameter("estimationEndDate", projDetails.getEstimationEndDate());
		query.setParameter("projectManager", projDetails.getProjectManager());
		query.setParameter("scrumMaster", projDetails.getScrumMaster());
		query.setParameter("projectName", projDetails.getProjectName());
		query.setParameter("startDate", projDetails.getStartDate());
		query.executeUpdate();
			System.out.println("am in update() of service impl");		
		//return "success";
	}

	
	

}
