package com.exa.pms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.exa.pms.dao.AddSprintDao;
import com.exa.pms.model.AddSprint;

/**
 * 
 * @author Pratima.Dolli
 *
 */
@Repository
public class AddSprintDaoImpl implements AddSprintDao{

	/**
	 * A persistence context handles a set of entities which hold data to be persisted in some persistence store
	 */
	@PersistenceContext
	EntityManager em;
	
	/*
	 * inserting AddSprint details into database
	 */
	
	@Override
	public AddSprint addSprintDetails(AddSprint addinfo) {
		
		System.out.println("Invoking Daoimplementation class");
		em.persist(addinfo);
		return addinfo;
	}

	/*
	 * getting AddSprint details from database
	 */
	
	@Override
	public List<AddSprint> getSprintDetails() {

		return em.createQuery("select a from AddSprint a",AddSprint.class).getResultList();
	}

}
