package com.exa.pms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.exa.pms.dao.BackLogDao;
import com.exa.pms.model.AddProjectDetails;
import com.exa.pms.model.BackLog;

@Repository("backLogDao")
public class BackLogDaoImpl implements BackLogDao{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public String insertBacklogDetails(BackLog backLog) {
		
		String res = "fail";
		Query query = em.createQuery("select b from BackLog b where b.backLogId=:backLogId ", BackLog.class);
		@SuppressWarnings("unchecked")
		List<BackLog> backlogId = (List<BackLog>) query.setParameter("backLogId", backLog.getBackLogId()).getResultList();
		if(backlogId.size() > 0){
			res = "fail";
		}else{
			System.out.println("invoking Dao Class");
			em.persist(backLog);
			System.out.println("success");
			res = "success";
		}
		return res;
		/*
		System.out.println("invoking Dao Class");
		em.persist(backLog);
		System.out.println("success");
		return "success";
*/	}

}
