/**
 * 
 */
package com.exa.pms.daoimpl;

import org.eclipse.persistence.sessions.factories.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.FileUploadDAO;
import com.exa.pms.model.UploadFile;


/**
 * @author Ravikiran J
 *
 */
@Repository
public class FileUploadDAOImpl implements FileUploadDAO {

	
	@PersistenceContext
	EntityManager em;

	public FileUploadDAOImpl() {
	}


	@Override
	@Transactional
	public void save(UploadFile uploadFile) {
		em.persist(uploadFile);
	}




}