/**
 * 
 */
package com.exa.pms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.GenericDao;

/**
 * @author Ravikiran J
 *
 *providing generic common implementation of GenericDao interface
 *Extend this abstract class to implement DAO for your specific needs
 */
public abstract class GenericJpaDao<T,ID> implements GenericDao<T, ID>{
	/**
	 *Implementing GenericDao interface to make available all the method present in GenericDao
	 * 
	 */


	private Class<T> persistantClass;

	/**
	 * The EntityManager is the primary interface used by
	 * application developers to interact with the JPA at runtime
	 */
	@PersistenceContext
	private EntityManager em;

	public GenericJpaDao(Class<T> persistantClass) {
		this.persistantClass=persistantClass;
	}

	public Class<T> getPersistantClass() {
		return persistantClass;
	}

	/**
	 * method to Find 
	 */
	@Transactional(readOnly=true)
	public T findById(ID id) {
		T entity=em.find(getPersistantClass(), id);
		return entity;
	}

	/**
	 * method to save entity
	 */
	@Transactional
	public T save(T entity) {
		em.persist(entity);
		return entity;
	}

	/**
	 * method to get the list
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<T> findAll(){
		return em.createQuery("select x from "+ getPersistantClass().getSimpleName()+" x ").getResultList();
	}

	/**
	 * method to update
	 */
	@Transactional
	public T update(T entity) {
		T mergedEntity=em.merge(entity);
		return mergedEntity;
	}

	/**
	 * Deletes all entities.
	 */
	@Transactional
	public void delete(T entity) {
		entity = this.em.merge(entity);
		this.em.remove(entity);
	}

	/**
	 * Delete entities by ID
	 */
	public void deleteById(ID id) {

	}

	public void flush() {
		em.flush();
	}

}


