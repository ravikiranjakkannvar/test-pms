package com.exa.pms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.HistoryDao;
import com.exa.pms.model.History;
import com.exa.pms.model.User;


/**
 * @author Sireesha.Palagiri
 *
 */
@Repository("historyDao")
public class HistoryDaoImpl implements HistoryDao {
	@PersistenceContext
	private EntityManager em;

	/*
	 * inserting recent changes into database
	 */
	@Override
	@Transactional
	public String insertHistory(History history) {
		String name = history.getName();
		Object[] userId = em.createQuery("select u.userId from User u where u.firstName=:name", User.class)
				.setParameter("name", name).getResultList().toArray();
		int uid1 = (int) userId[0];
		history.setUserId(uid1);
		em.persist(history);
		return "success";
	}
	/*
	 * fetching recent changes from database
	 */

	@Override
	public List<History> getHistory() {
		return em.createQuery("select t from History t", History.class).getResultList();
	}

}
