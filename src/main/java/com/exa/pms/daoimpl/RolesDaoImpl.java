package com.exa.pms.daoimpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.RolesDao;
import com.exa.pms.model.Roles;
/**
 * 
 * @author Pratima.Dolli
 *
 */
@Repository
@Transactional
public class RolesDaoImpl implements RolesDao {

	/**
	 * A persistence context handles a set of entities which hold data to be persisted in some persistence store
	 */
	
	@PersistenceContext
	EntityManager em;

	/*
	 * inserting AddSprint details into database
	 */
	
	@Override
	public Roles insertRoles(Roles role) {
		System.out.println("DaoImpl class");
		em.persist(role);
		return role;
	}

}
