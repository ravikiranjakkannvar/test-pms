/**
 * 
 */
package com.exa.pms.daoimpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.exa.pms.dao.Test1Dao;
import com.exa.pms.model.Test1;

/**
 * @author Ravikiran J
 *
 */
@Repository
public class Test1DaoImpl extends GenericJpaDao<Test1, Integer> implements Test1Dao {

	public Test1DaoImpl() {
		super(Test1.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public void delete(Integer id) {
		String query="delete from Test1 where id=:id ";
		em.createQuery(query,Test1.class).getResultList();
	}

}
