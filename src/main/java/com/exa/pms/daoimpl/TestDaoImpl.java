/**
 * 
 */
package com.exa.pms.daoimpl;


import org.springframework.stereotype.Repository;

import com.exa.pms.dao.TestDao;
import com.exa.pms.model.Test;

/**
 * @author Ravikiran J
 *
 */
@Repository
public class TestDaoImpl extends GenericJpaDao<Test, Integer> implements TestDao{

	public TestDaoImpl() {
		super(Test.class);
	}





}
