package com.exa.pms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.exa.pms.dao.UserDao;
import com.exa.pms.model.User;

/**
 * @author Sireesha.Palagiri
 *
 */
@Repository("userDao")
public class UserDaoImpl implements UserDao {
	@PersistenceContext
	private EntityManager em;

	/*
	 * inserting user details into database
	 */
	@SuppressWarnings({ "unlikely-arg-type", "unused" })
	@Override
	public String insertUser(User user) {
		String res;
		List<User> email = em.createQuery("select u.userMail from User u", User.class).getResultList();
		if (email.contains(user.getUserMail())) {
			return res = "fail";
		} else {
			em.persist(user);
			return res = "success";
		}
	}

	@Override
	public int update(User user) {

		User user1=em.find(User.class, user.getUserId());

		if(user1==null) {
			System.err.println("id not exist");
		}


		em.merge(user);
		return 1;
	}

}
