package com.exa.pms.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Repository;

import com.exa.pms.dao.UserLoginDao;
import com.exa.pms.model.User;

/**
 * @author Sireesha.Palagiri
 *
 */
@Repository("userLoginDao")
public class UserLoginDaoImpl implements UserLoginDao {
	@PersistenceContext
	private EntityManager em;

	/*
	 * Validating login details based on email and password
	 */
	@SuppressWarnings({ "unlikely-arg-type", "unused", "rawtypes", "unchecked" })
	@Override
	public String validateUser(User user) {
		String res = null;
		List<User> email = em.createQuery("select u.userMail from User u", User.class).getResultList();
		String getmail = user.getUserMail();

		List password = em.createQuery("select u.password from User u where u.userMail=:userMail", User.class)
				.setParameter("userMail", getmail).getResultList();
		String password1 = String.join(",", password);
		if (email.contains(user.getUserMail())) {
			if (user.getPassword().equals(password1)) {
				return "success";
			} else {
				return "password incorrect";
			}
		} else {
			return "not exist";
		}

	}

}