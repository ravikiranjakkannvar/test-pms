package com.exa.pms.initializer;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.exa.pms.config.WebConfig;

/**
 * We are providing the dispatcherservlet details and servlet Url mapping.
 * 
 * @author Dillip.Nayak
 *
 */
public class PmsInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	/**
	 * 
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] {};
	}

	/**
	 * Passing the dispatcher servlet required code.
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { WebConfig.class };
	}

	/**
	 * Defining the Url mapping.
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/*" };
	}

}
