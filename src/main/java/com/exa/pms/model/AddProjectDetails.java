package com.exa.pms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "addproject")
public class AddProjectDetails {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name = "projectId")
	private int projectId;
	
	@Column(name = "projectName")
	private String projectName;
	
	@Column(name = "projectManager")
	private String projectManager;
	
	
	@Column(name = "startDate")
	private String startDate;
	
	@Column(name = "estimationEndDate")
	private String estimationEndDate;
	
	@Column(name = "creationDate")
	private String creationDate;
	
	@Column(name = "scrumMaster")
	private String scrumMaster;

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEstimationEndDate() {
		return estimationEndDate;
	}

	public void setEstimationEndDate(String estimationEndDate) {
		this.estimationEndDate = estimationEndDate;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getScrumMaster() {
		return scrumMaster;
	}

	public void setScrumMaster(String scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	@Override
	public String toString() {
		return "AddProjectDetails [projectId=" + projectId + ", projectName=" + projectName + ", projectManager="
				+ projectManager + ", startDate=" + startDate + ", estimationEndDate=" + estimationEndDate
				+ ", creationDate=" + creationDate + ", scrumMaster=" + scrumMaster + "]";
	}
	
	
}
