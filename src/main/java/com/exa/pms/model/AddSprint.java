package com.exa.pms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity AddSprint This class represents a
 * database table
 * 
 * @author Pratima.Dolli
 *
 */
@Entity
@Table(name = "addsprint")
public class AddSprint {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "sprintId")
	private int sprintid;

	@Column(name = "projectId")
	private int projectid;

	@Column(name = "sprintName")
	private String sprintname;

	@Column(name = "startDate")
	private String startdate;

	@Column(name = "endDate")
	private String enddate;

	@Column(name = "efforts")
	private int efforts;

	// setters and getters method

	public int getSprintid() {
		return sprintid;
	}

	public void setSprintid(int sprintid) {
		this.sprintid = sprintid;
	}

	public int getProjectid() {
		return projectid;
	}

	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}

	public String getSprintname() {
		return sprintname;
	}

	public void setSprintname(String sprintname) {
		this.sprintname = sprintname;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public int getEfforts() {
		return efforts;
	}

	public void setEfforts(int efforts) {
		this.efforts = efforts;
	}

	//The toString() method returns the string representation of the object. 
	
	@Override
	public String toString() {
		return "AddSprint [sprintid=" + sprintid + ", projectid=" + projectid + ", sprintname=" + sprintname
				+ ", startdate=" + startdate + ", enddate=" + enddate + ", efforts=" + efforts + "]";
	}

}
