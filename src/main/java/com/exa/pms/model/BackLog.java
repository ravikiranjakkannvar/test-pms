package com.exa.pms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "backlogs")
public class BackLog {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name = "rank")
	private int rank;
	
	@Column(name = "backLogId")
	private String backLogId;
	
	@Column(name = "description")
	private String description;
	
	
	@Column(name = "planEstimate")
	private int planEstimate;
	
	@Column(name = "priority")
	private String priority;
	
	@Column(name = "owner")
	private String owner;

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getBackLogId() {
		return backLogId;
	}

	public void setBackLogId(String backLogId) {
		this.backLogId = backLogId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPlanEstimate() {
		return planEstimate;
	}

	public void setPlanEstimate(int planEstimate) {
		this.planEstimate = planEstimate;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "BackLog [rank=" + rank + ", backLogId=" + backLogId + ", description=" + description + ", planEstimate="
				+ planEstimate + ", priority=" + priority + ", owner=" + owner + "]";
	}
}
