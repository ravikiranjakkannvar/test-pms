/**
 * 
 */
package com.exa.pms.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base Entity Type to hold common id property to be extended
 * 
 * @author Ravikiran J
 *
 */
@MappedSuperclass
public class BaseEntity {

	@Id
	@GeneratedValue
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
