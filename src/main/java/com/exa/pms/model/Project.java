package com.exa.pms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Project
 * 
 * @author Dillip.Nayak
 *
 */
@Entity
@Table(name = "project")
public class Project {

	// It is the Primary key value
	@Id
	@GeneratedValue
	private int pId;

	//
	private String name;

	//
	private String description;

	/**
	 * generating getter for Id
	 * 
	 * @return
	 */
	public int getpId() {
		return pId;
	}

	/**
	 * we will use setters to set the value
	 * 
	 * @param pId
	 */
	public void setpId(int pId) {
		this.pId = pId;
	}

	/**
	 * generating getter for Name
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * we will use setters to set the value
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * generating getter for Description
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * we will use setters to set the value
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * To print the values of user defined variables in String format.
	 */
	@Override
	public String toString() {
		return "Project [pId=" + pId + ", name=" + name + ", description=" + description + "]";
	}

}
