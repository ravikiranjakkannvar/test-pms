package com.exa.pms.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "files_upload")
public class UploadFile {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;

	private List<MultipartFile> files;

	public UploadFile(){}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}


}