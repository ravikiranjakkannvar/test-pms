package com.exa.pms.service;

import java.util.List;

import com.exa.pms.model.AddProjectDetails;

public interface AddProjectDetailsService {
	
	/*
	 * 
	 */
	
	List<AddProjectDetails> getProjectDetails();
		
	/*
	 * 
	 */
	String insertProjectDetails(String projDetails);
	
	
	void updateProjectDetails(String projDetails);
	
	
	
	

}
