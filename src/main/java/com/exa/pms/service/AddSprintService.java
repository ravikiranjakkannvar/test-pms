package com.exa.pms.service;

import java.util.List;

import com.exa.pms.model.AddSprint;
/**
 * 
 * @author Pratima.Dolli
 *
 */
public interface AddSprintService {

	/**
	 * inserting sprint details into database
	 * 
	 * @param addinfo
	 * @return
	 */
	public AddSprint addSprintDetails(AddSprint addinfo);

	/*
	 *fetching details from database 
	 * 
	 * @return
	 */
	public List<AddSprint> getSprintDetails();

}
