package com.exa.pms.service;

import java.util.List;

import com.exa.pms.model.History;

public interface HistoryService {
	// inserting recent changes into database
	String insertHistory(String changes);

	// fetching recent changes from database
	List<History> getHistory();
}
