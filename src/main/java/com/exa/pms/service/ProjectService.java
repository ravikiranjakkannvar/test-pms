package com.exa.pms.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.ProjectDao;
import com.exa.pms.model.Project;

/**
 * 
 * @author Dillip.Nayak
 *
 */
@Service("projectService")
@Transactional
public class ProjectService {
	//
	@Autowired
	private ProjectDao projectDao;

	/**
	 * 
	 * @return
	 */
	public List<Project> getAllAdmin() {
		return projectDao.getAllEmployee();
	}
}