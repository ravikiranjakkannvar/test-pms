package com.exa.pms.service;

import com.exa.pms.model.Roles;
/**
 * 
 * @author Pratima.Dolli
 *
 */
public interface RolesService {

	/*
	 * inserting AddSprint details into database
	 */
	public Roles insertRoles(Roles role);

}
