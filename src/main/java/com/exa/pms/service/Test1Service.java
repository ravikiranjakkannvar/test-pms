package com.exa.pms.service;

import java.util.List;

import com.exa.pms.model.Test1;

public interface Test1Service {

	boolean createTest1(Test1 test1);

	public List<Test1> getList();

	public boolean update(Test1 test1);

	public Test1 findById(Integer id);

	public void delete(Test1 test1);
}
