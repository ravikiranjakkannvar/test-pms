/**
 * 
 */
package com.exa.pms.service;

import java.util.List;

import com.exa.pms.model.Test;

/**
 * @author Ravikiran J
 *
 */
public interface TestService {

	boolean createTest(Test test);

	public List<Test> getList();

	public boolean update(Test test);

	public Test findById(Integer id);

	public void delete(Test test);

}
