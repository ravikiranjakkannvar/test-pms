package com.exa.pms.service;

/**
 * @author Sireesha.Palagiri
 *
 */
public interface UserLoginService {
//Validating login details 
	String validateUser(String logindetails);
}
