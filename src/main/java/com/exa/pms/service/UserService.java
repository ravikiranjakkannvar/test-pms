package com.exa.pms.service;

import com.exa.pms.model.User;

/**
 * @author Sireesha.Palagiri
 *
 */
@SuppressWarnings("unused")
public interface UserService {
	// inserting user details into database
	String insertUser(String user);
	// update
	public int update(User user);

}
