package com.exa.pms.serviceimpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.AddProjectDetailsDao;
import com.exa.pms.model.AddProjectDetails;
import com.exa.pms.service.AddProjectDetailsService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


@Service
public class AddProjectDetailsServiceImpl implements AddProjectDetailsService{

	@Autowired
	private AddProjectDetailsDao addprojectDetailsDao;

	AddProjectDetails addProjectDetails= new AddProjectDetails();
	@Override
	@Transactional
	public List<AddProjectDetails> getProjectDetails() {
		System.out.println("am in pms1 class");

		return addprojectDetailsDao.getProjectDetailsList();
	}

	@Override
	@Transactional
	public String insertProjectDetails(String projDetails) {
		System.out.println("am in insertResult()");
		Gson gsonObj = new Gson();
		Map<String,Object> result = gsonObj.fromJson(projDetails, new TypeToken<Map<String,Object>>(){}.getType());

		System.out.println("Rows updated is : " + addProjectDetails.getProjectId());

		addProjectDetails.setCreationDate((String) result.get("creationDate"));
		addProjectDetails.setEstimationEndDate((String) result.get("estimationEndDate"));
		addProjectDetails.setProjectManager((String) result.get("projectManager"));
		addProjectDetails.setProjectName((String) result.get("projectName"));
		//addProjectDetails.setScrumMaster((String) result.get("scrumMaster"));
		addProjectDetails.setCreationDate((String) result.get("CreationDate"));
		addProjectDetails.setStartDate((String) result.get("startDate"));



		System.out.println(projDetails.toString());

		return addprojectDetailsDao.insertProjectDetails(addProjectDetails);
	}

	@Override
	@Transactional
	public void updateProjectDetails(String projDetails) {

		System.out.println("am in update() of add project");
		Gson gsonObj = new Gson();
		Map<String,Object> result = gsonObj.fromJson(projDetails, new TypeToken<Map<String,Object>>(){}.getType());

		System.out.println("Rows updated is : " + addProjectDetails.getProjectId());

		addProjectDetails.setCreationDate((String) result.get("creationDate"));
		addProjectDetails.setEstimationEndDate((String) result.get("estimationEndDate"));
		addProjectDetails.setProjectManager((String) result.get("projectManager"));
		//addProjectDetails.setProjectName((String) result.get("projectName"));
		addProjectDetails.setScrumMaster((String) result.get("scrumMaster"));
		addProjectDetails.setStartDate((String) result.get("startDate"));

		System.out.println(projDetails.toString());
		
		addprojectDetailsDao.updateProjectDetails(addProjectDetails);


	}
}
