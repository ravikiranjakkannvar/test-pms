package com.exa.pms.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.AddSprintDao;
import com.exa.pms.model.AddSprint;
import com.exa.pms.service.AddSprintService;

/**
 * 
 * @author Pratima.Dolli
 *
 */

@Service
@Transactional
public class AddSprintServiceImpl implements AddSprintService {

	/*
	 * Autowired enables we to inject the object dependency implicitly. It
	 * internally uses setter or constructor injection.
	 */

	@Autowired
	private AddSprintDao addSprintDao;

	/*
	 * inserting sprint details into database
	 * 
	 * @see com.exa.pms.service.AddSprintService#addSprintDetails(com.exa.pms.model.
	 * AddSprint)
	 */

	@Override
	public AddSprint addSprintDetails(AddSprint addinfo) {

		System.out.println("Invoking implementation class");
		return addSprintDao.addSprintDetails(addinfo);
	}

	/*
	 * getting sprint details from database
	 * 
	 * @see com.exa.pms.service.AddSprintService#getSprintDetails()
	 */

	@Override
	public List<AddSprint> getSprintDetails() {

		return addSprintDao.getSprintDetails();
	}

}
