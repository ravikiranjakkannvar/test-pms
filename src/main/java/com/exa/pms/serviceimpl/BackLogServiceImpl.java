package com.exa.pms.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.BackLogDao;
import com.exa.pms.model.BackLog;
import com.exa.pms.service.BackLogService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class BackLogServiceImpl implements BackLogService{

	@Autowired
	private BackLogDao backLogDao;
	
	BackLog backlogs=new BackLog();
	
	@Override
	@Transactional
	public String insertBacklogDetails(String addBackLogDetails) {
		System.out.println("am in insertBacklogDetails()");
		Gson gsonObj = new Gson();
		Map<String,Object> result = gsonObj.fromJson(addBackLogDetails, new TypeToken<Map<String,Object>>(){}.getType());

		System.out.println("Rows updated is : " + backlogs.getRank());

		backlogs.setBackLogId((String) result.get("backLogId"));
		backlogs.setDescription((String) result.get("description"));
		backlogs.setOwner((String) result.get("owner"));
		backlogs.setPlanEstimate(Integer.parseInt((String) result.get("planEstimate")));
		//addProjectDetails.setScrumMaster((String) result.get("scrumMaster"));
		backlogs.setPriority((String) result.get("priority"));
		//addProjectDetails.setStartDate((String) result.get("startDate"));



		System.out.println(addBackLogDetails.toString());
		return backLogDao.insertBacklogDetails(backlogs);
	}

}
