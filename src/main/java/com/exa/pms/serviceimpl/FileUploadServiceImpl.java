/**
 * 
 */
package com.exa.pms.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.FileUploadDAO;
import com.exa.pms.model.UploadFile;

/**
 * @author Ravikiran J
 *
 */
@Service
public class FileUploadServiceImpl {


	@Autowired
	private FileUploadDAO fileDao;

	@Transactional
	public void saveFile(UploadFile uploadFile) {
		fileDao.save(uploadFile);
	}
}
