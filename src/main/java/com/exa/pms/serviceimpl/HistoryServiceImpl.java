package com.exa.pms.serviceimpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.HistoryDao;
import com.exa.pms.dao.UserDao;
import com.exa.pms.model.History;
import com.exa.pms.model.User;
import com.exa.pms.service.HistoryService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author Sireesha.Palagiri
 *
 */
@Service
public class HistoryServiceImpl implements HistoryService {
	@Autowired
	private HistoryDao historyDao;

	/*
	 * inserting recent changes into database
	 */
	@Override
	@Transactional
	public String insertHistory(String changes) {
		Gson gsonObj = new Gson();
		Map<String, Object> gsonResult = gsonObj.fromJson(changes, new TypeToken<Map<String, Object>>() {
		}.getType());
		History history = new History();
		history.setName((String) gsonResult.get("name"));
		history.setComment((String) gsonResult.get("comment"));
		return historyDao.insertHistory(history);
	}
	/*
	 * fetching recent changes from database
	 */

	@Transactional
	@Override
	public List<History> getHistory() {
		return historyDao.getHistory();
	}

}
