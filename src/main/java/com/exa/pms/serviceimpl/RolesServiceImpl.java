package com.exa.pms.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exa.pms.dao.RolesDao;
import com.exa.pms.model.Roles;
import com.exa.pms.service.RolesService;

/**
 * 
 * @author Pratima.Dolli
 *
 */

@Service
public class RolesServiceImpl implements RolesService{

	@Autowired
	private RolesDao rolesDao;
	

	/*
	 * inserting sprint details into database
	 * 
	 * @see com.exa.pms.service.AddSprintService#addSprintDetails(com.exa.pms.model.
	 * AddSprint)
	 */
	@Override
	public Roles insertRoles(Roles role) {
		System.out.println("invoking Impl class");
		return rolesDao.insertRoles(role);
	}

}
