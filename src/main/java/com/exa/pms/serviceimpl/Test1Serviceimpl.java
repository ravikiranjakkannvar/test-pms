package com.exa.pms.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.Test1Dao;
import com.exa.pms.model.Test1;
import com.exa.pms.service.Test1Service;

@Service
public class Test1Serviceimpl implements Test1Service{

	@Autowired
	Test1Dao dao;

	@Transactional
	@Override
	public boolean createTest1(Test1 test1) {
		dao.save(test1);
		return true;
	}

	@Override
	public List<Test1> getList() {
		return dao.findAll();
	}

	@Override
	public boolean update(Test1 test1) {
		dao.update(test1);
		return true;
	}

	@Override
	public Test1 findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public void delete(Test1 test1) {


	}



}
