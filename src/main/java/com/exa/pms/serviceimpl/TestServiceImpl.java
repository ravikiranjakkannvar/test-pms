/**
 * 
 */
package com.exa.pms.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.TestDao;
import com.exa.pms.model.Test;
import com.exa.pms.service.TestService;

/**
 * @author Ravikiran J
 *
 */
@Service
public class TestServiceImpl implements TestService{

	@Autowired
	private TestDao testDao;

	/**
	 * creating test to insert into database
	 * 
	 */
	@Transactional
	@Override
	public boolean createTest(Test test) {
		testDao.save(test);
		return true;
	}


	@Override
	public List<Test> getList() {
		return testDao.findAll();
	}

	@Transactional
	@Override
	public boolean update(Test test) {
		testDao.update(test);
		return true;
	}


	@Override
	public Test findById(Integer id) {
		return testDao.findById(id);
	}


	@Override
	public void delete(Test test) {
		testDao.delete(test);

	}





}
