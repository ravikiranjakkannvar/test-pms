package com.exa.pms.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.exa.pms.dao.UserLoginDao;
import com.exa.pms.model.User;
import com.exa.pms.service.UserLoginService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author Sireesha.Palagiri
 *
 */
@Service
public class UserLoginServiceImpl implements UserLoginService {

	@Autowired
	private UserLoginDao userLoginDao;

	/*
	 * Validating login details based on email and password
	 */
	@Transactional
	@Override
	public String validateUser(String logindetails) {
		Gson gsonObj = new Gson();
		Map<String, Object> gsonResult = gsonObj.fromJson(logindetails, new TypeToken<Map<String, Object>>() {
		}.getType());
		User user1 = new User();
		user1.setUserMail((String) gsonResult.get("userMail"));
		user1.setPassword((String) gsonResult.get("password"));
		return userLoginDao.validateUser(user1);
	}
}
