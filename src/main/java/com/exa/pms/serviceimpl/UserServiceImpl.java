package com.exa.pms.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exa.pms.dao.UserDao;
import com.exa.pms.model.User;
import com.exa.pms.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author Sireesha.Palagiri
 *
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	/*
	 * inserting user details into database
	 */
	@Transactional
	@Override
	public String insertUser(String user) {
		Gson gsonObj = new Gson();
		Map<String, Object> gsonResult = gsonObj.fromJson(user, new TypeToken<Map<String, Object>>() {
		}.getType());
		User user1 = new User();
		user1.setUserMail((String) gsonResult.get("userMail"));
		user1.setFirstName((String) gsonResult.get("firstName"));
		user1.setLastName((String) gsonResult.get("lastName"));
		user1.setMobileNumber(Long.parseLong((String) gsonResult.get("mobileNumber")));
		user1.setPassword((String) gsonResult.get("password"));
		return userDao.insertUser(user1);
	}

	@Transactional
	@Override
	public int update(User user) {
		return userDao.update(user);
	}

}
