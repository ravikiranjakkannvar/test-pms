sap.ui.define([
               "sap/ui/core/mvc/Controller","com/exa/pms/service/BaseService","com/exa/pms/util/Formatters"
               ],function(Controller){
	"use strict";
	/**
	 * @fileOverview file for login
	 * @version 1.38.18
	 * @author Chaithra.kundar
	 *
	 */
	return Controller.extend("com.exa.pms.controller.Login",{
		onInit : function(){
			this._createAccnt = new com.exa.equiz.service.BaseService();
		},
		/**
		 * Function to handle sign in functionality
		 */
		onSignIn : function(){
			
			/*var sEmail = this.getView().byId("emailId").getValue();
			var sPassword = this.getView().byId("passWord").getValue();
			if(sEmail == "" || sPassword == "")
				{
				sap.m.MessageToast.show("Please enter the manatory fields");
				return;
				}else{
					var bEmailState = com.exa.pms.util.Formatters.eMailValidate(sEmail);
					if(!bEmailState)
						return;*/
					
					
			
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("overview");
			//	}
		},
		/**
		 * Function to open 'create account' fragment
		 */
		onCreateAccount : function(){
			if (!this._createAccntFragment) {
				this._createAccntFragment = sap.ui.xmlfragment("com.exa.pms.Fragments.createAccount", this);
				this.getView().addDependent(this._createAccntFragment);
			}
			this._createAccntFragment.open();
			this.clearAccntDetFileds();
		},
		/**
		 * Function to handle create account 
		 */
		OnCancel : function(){
			this._createAccntFragment.close();
		},
		/**
		 * Function to validate numbers
		 */
		phoneNumValidate : function(){
			var val = sap.ui.getCore().byId("mobNum").getValue();
			if(val == ""){
				sap.m.MessageToast.show("Please enter the manatory fields");
			}else{
				if (/^\d{10}$/.test(val)) {
					return true;
				} else {
					sap.m.MessageToast.show("Please enter the 10 digits mobile number");
					return false;
				}
			}
		},
		/**
		 * Function to handle email validation
		 */
		emailValidate : function(oEvent) {
			var sEmail = oEvent.getSource().getValue();
			com.exa.pms.util.Formatters.eMailValidate(sEmail);
		},
		/**
		 * Function to confirm entered password
		 */
		confirmPassword : function(){
			var sPassword = sap.ui.getCore().byId("createPasswrd").getValue();
			if(sPassword == ""){
				sap.m.MessageToast.show("Confirmation doesn't match password");
				sap.ui.getCore().byId("confirmPasswrd").setValue("");
				sap.ui.getCore().byId("confirmState").setVisible(false).setSrc("");
				return false;
			} else {
				var sConfrmPasswrd = sap.ui.getCore().byId("confirmPasswrd").getValue();
				if(sConfrmPasswrd == sPassword){
					sap.ui.getCore().byId("confirmState").setVisible(true).setSrc("image/right.png");
					return true;
				} else {
					sap.ui.getCore().byId("confirmState").setVisible(true).setSrc("image/wrong.jpg");

					sap.m.MessageToast.show("Confirmation doesn't match password");
					return false;
				}

			}
		},
		/**
		 * Function to create new account
		 */ 	
		OnCreateAccount:function(){
			var that = this;
			var fnError = function (data) {
				sap.m.MessageToast.show("create failed");
			};
			var fnSuccess = function (data) {
				if(data == true){
					sap.m.MessageToast.show("account created successfully");	
				} else {
					sap.m.MessageToast.show("The account already exists");	
				}
				that._createAccntFragment.close();
				that.clearAccntDetFileds();
			}
			var sEmail = sap.ui.getCore().byId("EmailCId").getValue();
			var sFirstName = sap.ui.getCore().byId("firstName").getValue();
			var sLastName = sap.ui.getCore().byId("lastName").getValue();
			var sPassword = sap.ui.getCore().byId("createPasswrd").getValue();
			var sMob = sap.ui.getCore().byId("mobNum").getValue();
			if(sFirstName == "" || sLastName == "" || sPassword == "" || sMob == "" || sEmail == "" ){
				sap.m.MessageToast.show("Please enter the manatory fields");
				return;
			}
			var bEmailState = com.exa.pms.util.Formatters.eMailValidate(sEmail);
			if(!bEmailState)
				return;
			if(!(this.phoneNumValidate()))
				return;

			if(!(this.confirmPassword())) {
				sap.m.MessageToast.show("Confirmation doesn't match password");
				return;
			}
			var inputPayload ={"userMail":sEmail,
					"firstName":sFirstName ,
					"lastName":sLastName,
					"mobileNumber":sMob,
					"password":sPassword
			}  ;


			var sUrl = "http://localhost:8080/pms/user/insertuser";
			this._createAccnt.executeAjax(sUrl, fnSuccess, fnError, "POST", inputPayload);	
		},
		/**
		 * Function to clear all fields data in account dailog
		 */
		clearAccntDetFileds : function(){
			sap.ui.getCore().byId("EmailCId").setValue("");
			sap.ui.getCore().byId("firstName").setValue("");
			sap.ui.getCore().byId("lastName").setValue("");
			sap.ui.getCore().byId("mobNum").setValue("");
			sap.ui.getCore().byId("createPasswrd").setValue("");
			sap.ui.getCore().byId("confirmPasswrd").setValue("");
			sap.ui.getCore().byId("confirmState").setVisible(false).setSrc("");
		}


	})
});