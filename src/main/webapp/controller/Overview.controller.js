sap.ui.define([
    "sap/ui/core/mvc/Controller"
], function(Controller) {

    return Controller.extend("com.exa.pms.controller.Overview", {
        onInit: function() {
        	that=this;
        },
        /**
         * Function to edit Project details
         */
        onProjectEdit : function(oEvent){
        	 if (!this._editProjFragment) {
                 this._editProjFragment = sap.ui.xmlfragment("com.exa.pms.Fragments.editProject", this);
                 this.getView().addDependent(this._editProjFragment);
             }
        	 this._editProjFragment.open(); 
        },

        /**
         * Function to open create project popover
         */
        handlePopoverProject: function(oEvent) {
            var Model = new sap.ui.model.json.JSONModel("model/projects.json");
            // create popover
            if (!this._oPopoverProject) {
                this._oPopoverProject = sap.ui.xmlfragment("com.exa.pms.Fragments.createProject", this);
                this.getView().addDependent(this._oPopoverProject);
            }
            sap.ui.getCore().byId("projectList").setModel(Model);
            // delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
            this.oButton = oEvent.getSource();
            jQuery.sap.delayedCall(100, this, function() {
                this._oPopoverProject.openBy(this.oButton);

            });
        },
        /**
         * Function to add overview fragment with respect to the selected project
         */
        onProjectListPress: function(oEvent) {
            this._oPopoverProject.close();
            if (this.overViewFragment == undefined) {
                this.overViewFragment = sap.ui.xmlfragment("com.exa.pms.Fragments.overView", this);
            }
            var oPage = this.getView().byId("overviewPage");
            oPage.insertContent(this.overViewFragment);
            this.burntDownGraph();
            //setting Model Into View
            var oDataModel = new sap.ui.model.json.JSONModel("model/data.json");
            sap.ui.getCore().byId("projectId").setModel(oDataModel);
            sap.ui.getCore().byId("recentChngesList").setModel(oDataModel);
        },
        /**
         * Function to draw burntDown graph
         */
        burntDownGraph: function() {
            //VizFrame declaration
            var oVizFrameLine = sap.ui.getCore().byId("idVizFrame");

            // setting vizFrame properties Title
            oVizFrameLine.setVizProperties({
                title: {
                    text: "BurntDown Chart"
                }
            })

            //Model creation 
            var oDataModel = new sap.ui.model.json.JSONModel("model/data.json");

            //setting Model Into vizframe
            oVizFrameLine.setModel(oDataModel);

            //VizFrame PopOver declaration
            var oPopOver = sap.ui.getCore().byId("idPopOver");
            oPopOver.connect(oVizFrameLine.getVizUid());



        },
        /**
         * Function to handle add project icon
         */
        handleAddProject: function(oEvent) {
            sap.ui.getCore().byId("addProjIcon").setVisible(false);
            sap.ui.getCore().byId("saveProjIcon").setVisible(true);
            sap.ui.getCore().byId("canclProjIcon").setVisible(true);
            sap.ui.getCore().byId("ProjectInput").setVisible(true);
            jQuery.sap.delayedCall(100, this, function() {
                this._oPopoverProject.openBy(this.oButton);

            });
        },
        /**
         * Function to handle cancel project icon
         */
        handleCancelProject: function() {
            sap.ui.getCore().byId("addProjIcon").setVisible(true);
            sap.ui.getCore().byId("saveProjIcon").setVisible(false);
            sap.ui.getCore().byId("canclProjIcon").setVisible(false);
            sap.ui.getCore().byId("ProjectInput").setVisible(false);
            jQuery.sap.delayedCall(100, this, function() {
                this._oPopoverProject.openBy(this.oButton);

            });
        },

        /**
         * Function to handle plan popover
         */
        handlePopoverPlan: function(oEvent) {
            var Model = new sap.ui.model.json.JSONModel("model/plan.json");
            // create popover
            if (!this._oPopoverPlan) {
                this._oPopoverPlan = sap.ui.xmlfragment("com.exa.pms.Fragments.createPlan", this);
                this.getView().addDependent(this._oPopoverPlan);
            }
            sap.ui.getCore().byId("planList").setModel(Model);
            // delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
            this.oButton = oEvent.getSource();
            jQuery.sap.delayedCall(100, this, function() {
                this._oPopoverPlan.openBy(this.oButton);

            });
        },
        /**
         * Function to navigate repective plans
         * @owner Date-11.09.2017 By-Fathima
         * @param oEvent
         */
        handlePlanItem: function(oEvent) {
            var sPlan = oEvent.getSource().getBindingContext().getObject().planName;
            switch (sPlan) {
                case "Backlogs":
                    this._oPopoverPlan.close();
                    if (this.backlogFragment == undefined) {
                        this.backlogFragment = sap.ui.xmlfragment("com.exa.pms.Fragments.createBacklog", this);
                        //this.getView().addDependent(this.backlogFragment);
                    }
                    var jsonModel = new sap.ui.model.json.JSONModel({
                        "backlogsData": [{
                                "rank": "1",
                                "id": "A134",
                                "description": "Upload User Image",
                                "planEstimated": "1",
                                "priority": "L",
                                "owner": ""
                            },
                            {
                                "rank": "2",
                                "id": "S234",
                                "description": "Edit User detail",
                                "planEstimated": "3",
                                "priority": "H",
                                "owner": ""
                            },
                            {
                                "rank": "3",
                                "id": "M1234",
                                "description": "Logout functionality",
                                "planEstimated": "8",
                                "priority": "L",
                                "owner": ""
                            },
                            {
                                "rank": "4",
                                "id": "W1234",
                                "description": "Mockups",
                                "planEstimated": "13",
                                "priority": "M",
                                "owner": ""
                            },
                        ],
                        "numberBacklogs" : 0,
                        "backlogDesc": ""
                    });
                    this.getView().setModel(jsonModel); //page
                    var model = this.getView().getModel();
                    model.getData().numberBacklogs = model.getData().backlogsData.length;
                    model.refresh(true);
                    var oPage = this.getView().byId("overviewPage");
                    oPage.removeContent(this.overViewFragment);
                    oPage.insertContent(this.backlogFragment);
                case "User Stories":
                case "Team Planning":
                case "Work Views":
            }
        },
        /**
         * Display for adding new backlog
         * @owner Date-11.09.2017 By-Fathima
         * @param oEvent
         */
        onAddNewBacklog: function(oEvent) {
            sap.ui.getCore().byId("idAddNewBacklogToolBar").setVisible(true);
            sap.ui.getCore().byId("idFilterBacklogToolBar").setVisible(false);
        },
        /**
         * Adding newly created backlog for main backlog Table
         * @owner Date-11.09.2017 By-Fathima
         * @param oEvent
         */
        onAddNewBacklogTable: function(oEvent) {
            sap.ui.getCore().byId("idAddNewBacklogToolBar").setVisible(false);
            var model = this.getView().getModel(),
                data = {
                    "rank": "",
                    "id": "W1234",
                    "description": model.getData().backlogDesc,
                    "planEstimated": "",
                    "priority": "",
                    "owner": ""
                };
            model.getData().backlogsData.push(data);
            model.getData().numberBacklogs = model.getData().backlogsData.length;
            model.getData().backlogDesc = "";
            model.refresh(true);
        },
        /**
         * Display filter toolbar for filtering backlogs
         * @owner Date-11.09.2017 By-Fathima
         * @param oEvent
         */
        onFilterBacklog: function(oEvent) {
            sap.ui.getCore().byId("idFilterBacklogToolBar").setVisible(true);
            sap.ui.getCore().byId("idAddNewBacklogToolBar").setVisible(false);
        },
        /**
         * Perform filter based on selected filter
         * @owner Date-11.09.2017 By-Fathima
         * @param oEvent
         */
        onFinalFilter: function(oEvent) {
            sap.ui.getCore().byId("idFilterBacklogToolBar").setVisible(false);
        },
        /**
         * Load additional detail fragments
         * @owner Date-11.09.2017 By-Fathima
         * @param oEvent
         */
        onProjectId : function(oEvent){
        	if (!this.backlogPaneFragment) {
                this.backlogPaneFragment = sap.ui.xmlfragment("com.exa.pms.view.backlogPanes", this);
                //this.getView().addDependent(this.backlogPaneFragment);
                //sap.ui.getCore().addDependent(this.backlogPaneFragment);
            }
        	sap.ui.getCore().byId("idPaneContainer").addPane(this.backlogPaneFragment);
        },
        
        
        onChange : function(oEvent) {
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var oCustomerHeaderToken = new UploadCollectionParameter({
				name : "x-csrf-token",
				value : "securityTokenFromModel"
			});
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
			MessageToast.show("Event change triggered");
		},

		onFileDeleted : function(oEvent) {
			MessageToast.show("Event fileDeleted triggered");
		},

		onFilenameLengthExceed : function(oEvent) {
			MessageToast.show("Event filenameLengthExceed triggered");
		},

		onFileSizeExceed : function(oEvent) {
			MessageToast.show("Event fileSizeExceed triggered");
		},

		onTypeMissmatch : function(oEvent) {
			MessageToast.show("Event typeMissmatch triggered");
		},
		onStartUpload : function(oEvent) {
			var oUploadCollection = this.getView().byId("UploadCollection");
			var oTextArea = this.getView().byId("TextArea");
			var cFiles = oUploadCollection.getItems().length;
			var uploadInfo = "";

			oUploadCollection.upload();

			uploadInfo = cFiles + " file(s)";
			if (oTextArea.getValue().length === 0) {
				uploadInfo = uploadInfo + " without notes";
			} else {
				uploadInfo = uploadInfo + " with notes";
			}

			MessageToast.show("Method Upload is called (" + uploadInfo + ")");
			sap.m.MessageBox.information("Uploaded " + uploadInfo);
			oTextArea.setValue("");
		},

		onBeforeUploadStarts : function(oEvent) {
			// Header Slug
			var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
				name : "slug",
				value : oEvent.getParameter("fileName")
			});
			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
			setTimeout(function() {
				MessageToast.show("Event beforeUploadStarts triggered");
			}, 4000);
		},

		onUploadComplete : function(oEvent) {
			var sUploadedFileName = oEvent.getParameter("files")[0].fileName;
			setTimeout(function() {
				var oUploadCollection = this.getView().byId("UploadCollection");

				for (var i = 0; i < oUploadCollection.getItems().length; i++) {
					if (oUploadCollection.getItems()[i].getFileName() === sUploadedFileName) {
						oUploadCollection.removeItem(oUploadCollection.getItems()[i]);
						break;
					}
				}

				// delay the success message in order to see other messages before
				MessageToast.show("Event uploadComplete triggered");
			}.bind(this), 8000);
		},
		/*--------------------- attachment fragment Diaplay( backlogAttachment ) -------------------------*/
		onAttachmentPress:function(oEvent){
			if(this.oDialog == undefined)
			{
				this.oDialog = sap.ui.xmlfragment(this.getView().getId(), "com.exa.pms.view.attachmentBaclogs",this);
				this.getView().addDependent(this.oDialog);
				//this.oDialog.setModel(oModel);
			}
			this.oDialog.open();
		},
		onAttachmentDialogCancel:function(){
			this.oDialog.close();
		}
    })
});