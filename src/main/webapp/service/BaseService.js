jQuery.sap.declare("com.exa.equiz.service.BaseService");

sap.ui.base.Object.extend("com.exa.equiz.service.BaseService", {
	
	executeAjax : function(sServicePath, fnSuccess, fnError, sRequetstType, oPayload){
		//var serviceURLForLocalData = "data/";
		var local = true;
		var sPayload = "";
		if(oPayload){
			sPayload = JSON.stringify(oPayload);
		}
		
		/*if(local1 && local){
			sServicePath = serviceURLForLocalData + jsonFileName;
			console.log("data coming from JSON");
		}
		*/
		$.ajax({
			type : sRequetstType,
			url : sServicePath,
			data : sPayload,
			dataType : "json",
			headers: {
				"content-type": "application/json",
				"Access-Control-Allow-Origin": "*",
				"cache-control": "no-cache"
			},
			async : false,
			 crossDomain: true,
			/* function(q){
				 alert("success");
			 },
			 function(a,b,c){
				 alert("fail");
			 }*/
		}).success(function( data ) {
			
			fnSuccess(data);
		}).fail(function(XMLHttpRequest, textStatus, errorThrown){
			if (textStatus == 'abort') {
				// The request has been aborted, probably by the facade because a new
				// request to the same URL has been triggered, so we
				// don't need to do anything.
				$.sap.log.debug("Request to service path '" + sServicePath + "' aborted");
			} else {
				$.sap.log.fatal("Error: The following problem occurred: " + textStatus, XMLHttpRequest.responseText + ","
						+ XMLHttpRequest.status + "," + XMLHttpRequest.statusText);

				// Call nested function
				if (fnError) {
					fnError(XMLHttpRequest, textStatus, errorThrown);
				}
			}
			fnError();
		});		
	}
});

