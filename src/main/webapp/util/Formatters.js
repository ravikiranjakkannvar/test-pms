jQuery.sap.declare("com.exa.pms.util.Formatters");

com.exa.pms.util.Formatters = {
	
		/**
		 * Function to handle email validation
		 */
		eMailValidate : function(oEmail) {
			if(oEmail == ""){
				sap.m.MessageToast.show("Please enter the manatory fields");
			}else{
			if(oEmail != undefined)
			var sEmailRegEx = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

			if (!oEmail.match(sEmailRegEx)) {
				sap.m.MessageToast.show("Please enter the valid Email ID");
				return false;
			} else {
				return true;
			}
			}
		},
}