/*package com.exa.pms.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.exa.pms.controller.ProjectController;
import com.exa.pms.model.Project;
import com.exa.pms.test.config.PMSTestConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

*//**
 * To test the java code using junit test controller
 * 
 * @author Dillip.Nayak
 *
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectControllerTest extends PMSTestConfig {

	// To inject the projectcontroller Object
	@Autowired
	private ProjectController projectController;

	// To Inject the Gson object
	@Autowired
	Gson gson;

	*//**
	 * To check the url and to test whether it is working as per our
	 * expectations
	 * 
	 * @throws Exception
	 *//*
	@Test
	public void testProjectDetails() throws Exception {
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(this.projectController).build();
		MockHttpServletResponse obj = mockMvc.perform(get("/projectname")).andExpect(status().isOk()).andReturn()
				.getResponse();
		List<Project> lsProject = gson.fromJson(obj.getContentAsString(), new TypeToken<List<Project>>() {
		}.getType());

		// By using assert we will check the expected value and actual value
		Assert.assertEquals(true, lsProject.size() > 0);
		Assert.assertNotNull(lsProject.get(0).getName());
		Assert.assertEquals("pms", lsProject.get(0).getName());
	}
}
*/